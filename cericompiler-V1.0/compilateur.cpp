//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>



using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND , POW, WTFM};
enum TYPE {INT, BOOL, WARNING};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&" | "^"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
	
TYPE Identifier(void){
	TYPE type = INT;
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void){
	TYPE type = INT;
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu !");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	     	else
				if(current==ID)
					type = Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue !");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&" | "^"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(), "*") == 0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(), "/") == 0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(), "%") == 0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(), "&&") == 0)
		opmul=AND;
	else if(strcmp(lexer->YYText(), "^") == 0)
		opmul=POW;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}

// 1*2*34*5*197

TYPE Term(void){
	int tagNbr = ++TagNumber;
	TYPE type;
	TYPE type2;
	OPMUL mulop;
	type = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2 = Factor();
		if(type != type2 )
		{
			Error("Types non compatibles.");
		}
		else
		{
			cout << "\tpop %rbx"<<endl;	// get second operand
			cout << "\tpop %rax"<<endl;	// get first operand
			switch(mulop)
			{
				case AND:
					if( type == BOOL and type2 == BOOL )
					{
						cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
						cout << "\tpush %rax\t# AND"<<endl;	// store result
						break;
					}
				case MUL:
					if( type != BOOL and type2 != BOOL)
					{
						cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
						cout << "\tpush %rax\t# MUL"<<endl;	// store result
						break;
					}
				case DIV:
					if( type != BOOL and type2 != BOOL)
					{
						cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
						cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
						cout << "\tpush %rax\t# DIV"<<endl;		// store result
						break;
					}
				case MOD:
					if( type != BOOL and type2 != BOOL)
					{
						cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
						cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
						cout << "\tpush %rdx\t# MOD"<<endl;		// store result
						break;
					}
				case POW:
					if( type != BOOL and type2 != BOOL)
					{
						cout << "\tmovq %rax, %rcx" << endl;
						cout << "POW0" << tagNbr << ":" << endl;
						cout << "\tcmpq $0, %rbx" << endl;
						cout << "\tjne BOUCLEPOW" << tagNbr << endl;
						cout << "\tpush $1" << endl;
						cout << "\tjmp ENDPOW" << tagNbr << endl;

						cout << "BOUCLEPOW" << tagNbr << ":" << endl;
						cout << "\tsubq $1, %rbx" << endl;
						cout << "\tcmpq $0, %rbx" << endl;
						cout << "\tje RESULTPOW" << tagNbr << endl;
						cout << "\tmulq %rcx" << endl;
						cout << "\tjmp BOUCLEPOW" << tagNbr << endl;
						cout << "RESULTPOW" << tagNbr << ":" << endl;
						cout << "\tpush %rax" << endl;

						cout << "ENDPOW" << tagNbr << ":" << endl;
						break;
					}
				default:
					Error("opérateur multiplicatif attendu !");
			}
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE type;
	TYPE type2;
	OPADD adop;
	type = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		if(type != type2)
		{
			Error("Types non compatibles.");
		}
		
		else
		{
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(adop){
				case OR:
					if(type == BOOL && type2 == BOOL)
					{
						cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
						break;
					}			
				case ADD:
					if(type != BOOL && type2 != BOOL)
					{
						cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
						break;
					}			
				case SUB:
					if(type != BOOL && type2 != BOOL)
					{
						cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
						break;
					}
				default:
					Error("opérateur additif inconnu !");
			}
			cout << "\tpush %rax"<<endl;			// store result
		}
		
	}
	return type;

}


void DisplayStatement(void){
	int tagNbr = ++TagNumber;
	TYPE type;
	if(current == KEYWORD && strcmp(lexer->YYText(), "DISPLAY") == 0)
	{
		current=(TOKEN) lexer->yylex();
		cout << "DISPLAY" << tagNbr << ":" << endl;
		type = Expression();
		if(type == INT){
			cout << "\tpop %rdx\t#the value to be displayed" << endl;
			cout << "\tmovq $FormatString1, %rsi\t#\"%llu\\n\"" << endl;
			cout << "\tmovl $1,%edi" << endl;
			cout << "\tmovl $0,%eax" << endl;
			cout << "\tcall __printf_chk@PLT" << endl;
		}
	}	
}


// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t#used by printf to display 64 bit unsigned integers"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE type;
	OPREL oprel;
	type =SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu !");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOL;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	TYPE type = Expression();
	if(type != INT)
	{
		Error("L'expression doit etre de type INT !");
	}
	cout << "\tpop "<<variable<<endl;
	return variable;
}

void Statement(void);

// IfStatement := "IF" Expression "THEN" Statement
// { "ELSEIF Expression "THEN" Statement }
// [ "ELSE" "THEN" Statement }
void IfStatement(void){
	int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "IF") == 0)
	{

		cout << "IF" << tagNbr << ":" << endl;
		current = (TOKEN) lexer->yylex();
		TYPE type = Expression();
		if(type != BOOL)
		{
			Error("Type BOOL attendu pour l'expression du IF !");
		}
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tjz ELSEIF" << tagNbr << endl;

		if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
		{
			cout << "THEN" << tagNbr << ":" << endl;
			current = (TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp ENDIF" << tagNbr << endl;
		}
		else
		{
			Error("THEN attendu !");
		}

		cout << "ELSEIF" << tagNbr << ":" << endl;
		int cpt = 0;
		while(current == KEYWORD && strcmp(lexer->YYText(), "ELSEIF") == 0)
		{
			cpt++;
			current = (TOKEN)lexer->yylex();
			TYPE type = Expression();
			if(type != BOOL)
			{
				Error("Type BOOL attendu pour l'expression du ELSEIF!");
			}
			cout << "\tpop %rax" << endl;
			cout << "\tcmpq $0, %rax" << endl;
			cout << "\tje ENDELSEIF" << tagNbr << "_" << cpt << endl;
			if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
			{	
				cout << "ELSEIFTHEN" << tagNbr << "_" << cpt << ":" << endl;
				current = (TOKEN) lexer->yylex();
				Statement();
				cout << "\tjmp ENDIF" << tagNbr << endl;
			}
			else
			{
				Error("Then attendu dans le ESLEIF !");
			}
			cout << "ENDELSEIF" << tagNbr << "_" << cpt << ":" << endl;

		}



		cout << "ELSE" << tagNbr << ":" << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "ELSE") == 0)
		{
			current = (TOKEN)lexer->yylex();
			if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
			{
				cout << "ELSETHEN" << tagNbr << ":" << endl;
				current = (TOKEN)lexer->yylex();
				Statement();
			}
			else
			{
				Error("THEN attendu dans le ELSE !");
			}
		}
	}
	else
	{
		Error("IF attendu !");
	}
	cout << "ENDIF" << tagNbr << ":" << endl;
}

// SwitchStatement := "SWITCH" Expression { "CASE" Expression "DO" Statement }
void SwitchStatement(void){
	int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "SWITCH") == 0)
	{
		cout << "SWITCH" << tagNbr << ":" << endl;
		current = (TOKEN) lexer->yylex();
		TYPE type = Expression();
		if(type == BOOL)
		{
			Error("Le switch ne fonctionne pas avec un BOOL !");
		}
		cout << "\tpop %rax" << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "CASE") == 0)
		{
			int cpt = 0;
			while(current == KEYWORD && strcmp(lexer->YYText(), "CASE") == 0)
			{
				cpt++;
				cout << "CASE" << tagNbr << "_" << cpt << ":" << endl;
				current = (TOKEN) lexer->yylex();
				TYPE type2 = Expression();
				if(type2 == BOOL)
				{
					Error("Le CASE du SWITCH ne fonctionne pas avec un BOOL !");
				}
				cout << "\tpop %rbx" << endl;
				cout << "\tcmpq %rax, %rbx" << endl;
				cout << "\tjne ENDCASE" << tagNbr << "_" << cpt << endl;
				if(current == KEYWORD && strcmp(lexer->YYText(), "DO")  == 0)
				{
					cout << "DO" << tagNbr << "_" << cpt << ":" << endl;
					current = (TOKEN) lexer->yylex();
					Statement();
					cout << "\tjmp ENDSWITCH" << tagNbr << endl;
				}
				else
				{
					Error("DO attendu dans le CASE du SWITCH !");
				}

				cout << "ENDCASE" << tagNbr << "_" << cpt << ":" << endl;
			}
		}
		else
		{
			Error("CASE attendu !");
		}
	}
	else
	{
		Error("SWITCH attendu !");
	}
	cout << "ENDSWITCH" << tagNbr << ":" << endl;
}

// WhileStatement := "WhileStatement" Expression "DO" Statement
void WhileStatement(void){
	int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "WHILE") == 0)
	{
		cout << "WHILE" << tagNbr << ":" << endl;
		current = (TOKEN) lexer->yylex();
		TYPE type = Expression();
		if(type != BOOL){
			Error("L'expression n'est pas un BOOL");
		}
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tjz ENDWHILE" << tagNbr << endl;
		
		if(current == KEYWORD && strcmp(lexer->YYText(), "DO") == 0)
		{
			cout << "DO" << tagNbr << ":" << endl;
			current = (TOKEN) lexer->yylex();
			Statement();
		}
		else
		{
			Error("DO attendu !");
		}
	}
	else
	{
		Error("WHILE attendu !");
	}
	cout << "\tjmp WHILE" << tagNbr << endl;
	cout << "ENDWHILE" << tagNbr << ":" << endl;
}

// ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
void ForStatement(void){
	int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "FOR") == 0)
	{
		cout << "FOR" << tagNbr << ":" << endl;
		current = (TOKEN) lexer->yylex();
		string var = AssignementStatement();

		
		if(current == KEYWORD && strcmp(lexer->YYText(), "TO") == 0)
		{
			cout << "TO" << tagNbr << ":" << endl;
			current = (TOKEN) lexer->yylex();
			cout << "\tpush " << var << endl;
			cout << "\tpop %rax\t" << endl;
			TYPE type = Expression();
			if(type != INT){
				Error("L'expression n'est pas un INT !");
			}
			cout << "\tpop %rbx\t#Résultat de l'expression dans la pile" << endl;
			cout << "\tcmpq %rax, %rbx" << endl;
			cout << "\tje ENDFOR" << tagNbr << "\t#Si egal jump a la fin du for" << endl;

			if(current == KEYWORD && strcmp(lexer->YYText(), "DO") == 0)
			{
				cout << "DO" << tagNbr << ":" << endl;
				current = (TOKEN) lexer->yylex();
				Statement();
				cout << "\tpush " << var << endl;
				cout << "\tpop %rax\t" << endl;
				cout << "\taddq $1, %rax" << endl;
				cout << "\tpush %rax" << endl;
				cout << "\tpop " << var << endl;
			
			}	
			else
			{
				Error("DO attendu !");
			}
		}	
		else
		{
			Error("TO attendu !");
		}
	}
	else
	{
		Error("FOR attendu !");
	}
	cout << "\tjmp TO" << tagNbr << endl;
	cout << "ENDFOR" << tagNbr << ":" << endl;
}

// BlockStatement := "BEGIN" Expression { ";" Expression } "END"
void BlockStatement(void){
	int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "BEGIN") == 0)
	{
		cout << "BEGIN" << tagNbr << ":" << endl;
		current = (TOKEN) lexer->yylex();
		Statement();
		
		while(strcmp(lexer->YYText(), ";") == 0)
		{
			current = (TOKEN) lexer->yylex();
			Statement();
		}
		
		if(current == KEYWORD && strcmp(lexer->YYText(), "END") == 0)
		{
			cout << "END" << tagNbr << ":" << endl;
			current = (TOKEN) lexer->yylex();
		}
		
		else
		{
			Error("END attendu !");
		}
	}
	else
	{
		Error("BEGIN attendu !");
	}
}

// Statement := AssignementStatement
void Statement(void){
	if(current == ID)
		AssignementStatement();
	else if(strcmp(lexer->YYText(), "IF") == 0)
		IfStatement();
	else if(strcmp(lexer->YYText(), "WHILE") == 0)
		WhileStatement();
	else if(strcmp(lexer->YYText(), "FOR") == 0)
		ForStatement();
	else if(strcmp(lexer->YYText(), "BEGIN") == 0)
		BlockStatement();
	else if(strcmp(lexer->YYText(), "DISPLAY") == 0)
		DisplayStatement();
	else if(strcmp(lexer->YYText(), "SWITCH") == 0)
		SwitchStatement();
	else
		Error("Statement error !");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu !");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			






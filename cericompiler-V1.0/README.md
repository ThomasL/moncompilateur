# Compilateur Final LEGRAND Thomas Licence 2 Informatique CERI

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)
by : LEGRAND Thomas

**Tout d'abord je souhaite signaler que j'ai travaillé à partir du corrigé des TP1&2, ainsi, à partir du TP3, toutes les fonctions et ajouts ont étés réalisés par mes soins.**

**The basic version could handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Identifier ":=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

# Ajouts :

## Création d'une fonction DISPLAY
Qui nous aidera par la suite à tester plus facilement les autres fonctions et ajouts.

### Construction :

	DisplayStatement :=DISPLAY Expression

### Exemple :

	[a]
	a:=1;
	DISPLAY a;
	DISPLAY 2
	.

### Résultat :

	1
	2


## Création d'un BlockStatement, permettant de donner plusieurs instructions là où ce n'est pas possible de base :
Utile notamment dans la fonction FOR, que nous verront plus tard.

### Construction :

	BlockStatement := "BEGIN" Expression { ";" Expression } "END"

### Exemple :

	[a]
	FOR a:=0
	TO 5
	DO 
	BEGIN
	a := a+1;
	DISPLAY a
	END
	.

### Résultat :

	0
	1
	2
	3
	4


## Ajout du ELSEIF dans la fonction IF

### Construction :

	IfStatement := "IF" Expression "THEN" Statement
					{ "ELSEIF Expression "THEN" Statement }
					[ "ELSE" "THEN" Statement }

### Exemple :

	[a]
	a := 1;
	IF a == 1 THEN
	DISPLAY 10
	ELSEIF a == 2 THEN
	DISPLAY 20
	ELSE THEN
	DISPLAY 0
	.

### Résultat :

	10


## Création de la fonction WHILE

### Construction :

	WhileStatement := "WhileStatement" Expression "DO" Statement

### Exemple :

	[a]
	a := 0;
	WHILE a < 5 DO
	BEGIN
	a := a+1;
	DISPLAY a
	END
	.

### Résultat :

	1
	2
	3
	4
	5


## Création de la fonction FOR

### Construction :

	ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement

### Exemple :

	[a]
	FOR a:=0
	TO 5
	DO 
	BEGIN
	a := a+1;
	DISPLAY a
	END
	.

### Résultat :

	0
	1
	2
	3
	4


## Création d'une fonction SWITCH / CASE qui peut remplacer le IF / ELSEIF

### Construction :

	SwitchStatement := "SWITCH" Expression { "CASE" Expression "DO" Statement }

### Exemple :

	[a,b,c,d]
	a:=3;
	b:=11;
	c:=22;
	d:=33;
	SWITCH a
	CASE 1 DO DISPLAY b
	CASE 2 DO DISPLAY c
	CASE 3 DO DISPLAY d
	.

### Résultat :

	33


## Ajout d'un MultiplicativeOpérator ^ qui permet de calculer une puissance

### Exemple :

	[a,b]
	a := 4^8;
	DISPLAY a
	.

### Résultat :

	65536

## Dernières précisions :

A de nombreux endroits, TYPE remplace void, ainsi, on return le TYPE adéquat (BOOL ou INT) pour pouvoir return le type de l'expression par exemple, afin de pouvoir mettre une Erreur en cas de types non voulus. (Exemple : Affichage d'une erreur si l'on veut faire un IF avec un Booléen)
Ces cas d'erreurs sont gérés dans toutes les fonctions ce qui permet de savoir le problème et de stopper l'exécution du code en toutes circonstances.

			# This code was produced by the CERI Compiler
	.data
FormatString1:	.string "%llu\n"	#used by printf to display 64 bit unsigned integers
	.align 8
a:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop a
WHILE2:
	push a
	push $5
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai5	# If below
	push $0		# False
	jmp Suite5
Vrai5:	push $0xFFFFFFFFFFFFFFFF		# True
Suite5:
	pop %rax
	cmpq $0, %rax
	jz ENDWHILE2
DO2:
BEGIN6:
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
DISPLAY9:
	push a
	pop %rdx	#the value to be displayed
	movq $FormatString1, %rsi	#"%llu\n"
	movl $1,%edi
	movl $0,%eax
	call __printf_chk@PLT
END6:
	jmp WHILE2
ENDWHILE2:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
